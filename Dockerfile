FROM ubuntu:14.04

RUN apt-get update &&\
    apt-get -y install software-properties-common &&\
    apt-add-repository ppa:brightbox/ruby-ng &&\
    apt-get update &&\
    apt-get -y install build-essential ruby2.2 ruby2.2-dev libsqlite3-dev &&\
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN gem install mailcatcher --no-rdoc --no-ri

EXPOSE 1025 1080

CMD ["mailcatcher", "-f", "--http-ip=0.0.0.0"]
